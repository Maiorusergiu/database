package com.example.structureapp.experiments;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class experimentsService {
    private final experimentsRepository experimentsRepo;
    @Autowired
    public experimentsService(experimentsRepository experimentsRepo){
        this.experimentsRepo = experimentsRepo;
    }

    public List<experiments> getExperiments() {
        return experimentsRepo.findAll();
    }
    public experiments addExperiments(experiments experiments){
        return experimentsRepo.save(experiments);
    }
    public experiments updateExperiments(experiments experiments){
        return experimentsRepo.save(experiments);
    }
    public experiments getExperimentById(Long id) {
        return experimentsRepo.findById(id).
        orElseThrow(() -> new ExperimentsNotFoundException("The experiment by id " + id + " was not found" ));
    }



    public void delete(Long id) {
        experimentsRepo.deleteById(id);
    }

    
}
