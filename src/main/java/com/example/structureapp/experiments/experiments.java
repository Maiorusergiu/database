package com.example.structureapp.experiments;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.example.structureapp.sessions.sessions;
import com.example.structureapp.stimuli.stimuli;
import com.fasterxml.jackson.annotation.JsonBackReference;




@Entity
public class experiments implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long pkExperiments;
    private String responseDirection;
    @ManyToOne
    @JoinColumn(name = "fkSession")
    private sessions sessions;
    @ManyToOne
    @JoinColumn(name = "fkStimuli")
    private stimuli stimuli;

    @JsonBackReference(value = "experiments_stimuli")
    public stimuli getStimuli() {
        return stimuli;
    }

    public void setStimuli(stimuli stimuli) {
        this.stimuli = stimuli;
    }

    public experiments() {

    }

    @JsonBackReference(value = "experiments_sessions")
    public sessions getSessions() {
        return sessions;
    }

    public void setSessions(sessions sessions){
        this.sessions = sessions;
    }
    
    public experiments(Long pkExperiments, String responseDirection) {
        this.pkExperiments = pkExperiments;
        this.responseDirection = responseDirection;
    }

    public Long getpkExperiments() {
        return pkExperiments;
    }

    public void setpkExperiments(Long pkExperiments) {
        this.pkExperiments = pkExperiments;
    }

    public String getResponseDirection() {
        return responseDirection;
    }

    public void setResponseDirection(String responseDirection) {
        this.responseDirection = responseDirection;
    }




    
}
