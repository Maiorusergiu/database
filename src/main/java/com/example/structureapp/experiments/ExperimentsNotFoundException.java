package com.example.structureapp.experiments;

public class ExperimentsNotFoundException extends RuntimeException {
    public ExperimentsNotFoundException(String message) {
        super(message);
    }
}
