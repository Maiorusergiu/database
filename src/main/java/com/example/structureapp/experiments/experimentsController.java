package com.example.structureapp.experiments;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/experiments")
public class experimentsController {
    private final experimentsService experimentsServ;

    public experimentsController(experimentsService experimentsServ) {
        this.experimentsServ = experimentsServ;
    }



    @GetMapping("/all")
    public ResponseEntity<List<experiments>> getAllExperiments() {
        List<experiments> experiments = experimentsServ.getExperiments();
        return new ResponseEntity<>(experiments, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<experiments> addExperiment(@RequestBody experiments experiment) {
        experiments newExperiment = experimentsServ.addExperiments(experiment);
        return new ResponseEntity<>(newExperiment, HttpStatus.CREATED);
    }

    @GetMapping("find/{id}")
    public ResponseEntity<experiments> findExperimentById(@PathVariable("id") Long id){
        experiments experiment = experimentsServ.getExperimentById(id);
        return new ResponseEntity<>(experiment, HttpStatus.OK);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<experiments> deleteExperiment(@PathVariable("id") Long id){
        experimentsServ.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
