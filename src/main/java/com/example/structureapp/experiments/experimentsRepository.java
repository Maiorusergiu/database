package com.example.structureapp.experiments;

import org.springframework.data.jpa.repository.JpaRepository;

public interface experimentsRepository extends JpaRepository<experiments, Long> {
    
}
