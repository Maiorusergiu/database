package com.example.structureapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StructureappApplication {

	public static void main(String[] args) {

		SpringApplication.run(StructureappApplication.class, args);

		
		
	}

}
