package com.example.structureapp.sessions;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sessions")
public class sessionsController {
    private final sessionsService sessionsServ;

    public sessionsController(sessionsService sessionServ) {
        this.sessionsServ = sessionServ;
    }

    @GetMapping("/all")
    public ResponseEntity<List<sessions>> getSessions() {
        List<sessions> sessions = sessionsServ.getAllSessions();
        return new ResponseEntity<>(sessions, HttpStatus.OK);
    }

    @GetMapping("find/{sessionsId}")
    public ResponseEntity<sessions> getSessionsById(@PathVariable("sessionsId") Long sessionsId) {
        sessionsServ.getSessionsById(sessionsId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<sessions> addSession(@RequestBody sessions session){
        sessions newSession = sessionsServ.addSession(session);
        return new ResponseEntity<>(newSession, HttpStatus.CREATED);
    }

    @PostMapping("/update")
    public ResponseEntity<sessions> updateSession(@RequestBody sessions session){
        sessions updateSession = sessionsServ.updateSession(session);
        return new ResponseEntity<>(updateSession, HttpStatus.CREATED);
    }


    @DeleteMapping("delete/{id}")
    public ResponseEntity<sessions> deleteSession(@PathVariable("id")  Long id) {
        sessionsServ.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
