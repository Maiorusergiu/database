package com.example.structureapp.sessions;

import java.io.Serializable;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.example.structureapp.experiments.experiments;
import com.example.structureapp.movements.movements;
import com.example.structureapp.participants.participants;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class sessions implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long pkSessions;
    private String date;
    private String time;
    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sessions")
    private List<experiments> experiments;

    @ManyToOne
    @JoinColumn(name = "fkParticipants")
    private participants participants;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sessions")
    private List<movements> movements;




    @JsonManagedReference(value = "movements_sessions")
    public List<movements> getMovements() {
        return movements;
    }
    public void setMovements(List<movements> movements) {
        this.movements = movements;
    }

    @JsonBackReference(value = "sessions_participants")
    public participants getParticipants() {
        return participants;
    }

    public void setParticipants(participants participants) {
        this.participants = participants;
    }

    @JsonManagedReference(value = "experiments_sessions")
    public List<experiments> getExperiments() {
        return experiments;
    }
    

    public void setExperiments(List<experiments> experiments) {
        this.experiments = experiments;
    }
    
    public sessions() {
        
    }
    public sessions(Long pkSessions, String date, String time, String description) {

        this.pkSessions = pkSessions;
        this.date = date;
        this.time = time;
        this.description = description;
    }

    public Long getpkSessions() {
        return pkSessions;
    }

    public void setPkSessions(Long pkSessions) {
        this.pkSessions = pkSessions;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date){
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time){
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }






    
}
