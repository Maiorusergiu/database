package com.example.structureapp.sessions;

import org.springframework.data.jpa.repository.JpaRepository;

public interface sessionsRepository extends JpaRepository<sessions, Long> {
    
}
