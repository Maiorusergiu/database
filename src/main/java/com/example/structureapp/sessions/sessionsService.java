package com.example.structureapp.sessions;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class sessionsService {
    private final sessionsRepository sessionsRepo;
    @Autowired
    public sessionsService(sessionsRepository sessionsRepo) {
        this.sessionsRepo = sessionsRepo;
    }

    public sessions addSession(sessions sessions) {
        return sessionsRepo.save(sessions);
    }

    public List<sessions> getAllSessions() {
        return sessionsRepo.findAll();
    }

    public sessions getSessionsById(Long id) {
        return sessionsRepo.findById(id)
        .orElseThrow(() -> new SessionsNotFoundException("Session by id " + id + " was not found"));
    }

    public sessions updateSession(sessions session) {
        return sessionsRepo.save(session);
    }

    public void delete(Long id) {
        sessionsRepo.deleteById(id);
    }

    
}
