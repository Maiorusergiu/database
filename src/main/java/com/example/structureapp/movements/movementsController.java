package com.example.structureapp.movements;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/movements")
public class movementsController {
    private final movementsService serv;
    @Autowired
    public movementsController(movementsService serv) {
        this.serv = serv;
    }

    @GetMapping("/all")
    public ResponseEntity<List<movements>> getAllMovements() {
        List<movements> movements = serv.getAllMovements();
        return new ResponseEntity<>(movements, HttpStatus.OK);
    }

    @GetMapping("find/{id}")
    public ResponseEntity<movements> getMovementById(@PathVariable("id") Long id) {
        movements movementById = serv.getMovementById(id);
        return new ResponseEntity<>(movementById, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<movements> addMovement(@RequestBody movements movement){
        movements newMovement = serv.addMovement(movement);
        return new ResponseEntity<>(newMovement, HttpStatus.CREATED);
    }
    @PostMapping("/update")
    public ResponseEntity<movements> updateMovement(@RequestBody movements movement){
        movements updateMovement = serv.addMovement(movement);
        return new ResponseEntity<>(updateMovement, HttpStatus.CREATED);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<movements> deleteMovement(@PathVariable("id") Long id){
        serv.deleteMovement(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
