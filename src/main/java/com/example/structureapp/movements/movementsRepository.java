package com.example.structureapp.movements;

import org.springframework.data.jpa.repository.JpaRepository;

public interface movementsRepository extends JpaRepository<movements, Long> {
    
}
