package com.example.structureapp.movements;

public class MovementsNotFoundException extends RuntimeException {
    public MovementsNotFoundException(String message) {
        super(message);
    }
}
