package com.example.structureapp.movements;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.ManyToOne;

import com.example.structureapp.frames.frames;
import com.example.structureapp.sessions.sessions;
import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
public class movements implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long pkMovements;
    private Integer XCoordinate;
    private Integer YCoordinate;
    private String Screen;
    @ManyToOne
    @JoinColumn(name = "fkSessions")
    private sessions sessions;
    @ManyToOne
    @JoinColumn(name = "fkFrames")
    private frames frames;



@JsonBackReference(value="framesMovements")
public frames getFrames() {
    return frames;
}

public void setFrames(frames frames) {
    this.frames = frames;
}


    @JsonBackReference(value = "movements_sessions")
    public sessions getSessions() {
        return sessions;
    }
    public void setSessions(sessions sessions) {
        this.sessions = sessions;
    }


    public movements() {

    }

    public movements(Long pkMovements, Integer XCoordinate, Integer YCoordinate, String Screen) {
        this.pkMovements = pkMovements;
        this.XCoordinate = XCoordinate;
        this.YCoordinate = YCoordinate;
    }

    public Long getId() {
        return pkMovements;
    }

    public void setId(Long pkMovements) {
        this.pkMovements = pkMovements;
    }
    public Integer getXCoordinate() {
        return XCoordinate;
    }

    public void setXCoordinate(Integer XCoordinate){
        this.XCoordinate = XCoordinate;
    }

    public Integer getYCoordinate() {
        return YCoordinate;
    }

    public void setYCoordinate(Integer YCoordinate) {
        this.YCoordinate = YCoordinate;
    }

    public String getScreen() {
        return Screen;
    }

    public void setScreen(String Screen) {
        this.Screen = Screen;
    }


    

}
