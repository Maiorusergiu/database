package com.example.structureapp.movements;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class movementsService {
    private final movementsRepository repo;
    @Autowired
    public movementsService(movementsRepository repo) {
        this.repo = repo;
    }

    public List<movements> getAllMovements() {
        return repo.findAll();
    }

    public movements getMovementById(Long id) {
        return repo.findById(id).orElseThrow(() -> new MovementsNotFoundException("Movement by id " + id + " was not found"));
    }

    public movements addMovement(movements movement) {
        return repo.save(movement);
    }

    public movements updateMovement(movements updateMovement){
        return repo.save(updateMovement);
    }

    public void deleteMovement(Long id) {
        repo.deleteById(id);
    }
    
}
