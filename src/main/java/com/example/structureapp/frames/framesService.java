package com.example.structureapp.frames;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class framesService {
    private final framesRepository framesRepo;
    @Autowired
    public framesService(framesRepository framesRepo) {
        this.framesRepo = framesRepo;
    }

    public List<frames> getAllFrames() {
        return framesRepo.findAll();
    }

    public frames addFrame(frames addFrame) {
        return framesRepo.save(addFrame);
    }

    public frames updateFrame(frames updateFrame){
        return framesRepo.save(updateFrame);
    }

    public frames getFrameById(Long id) {
        return framesRepo.findById(id).orElseThrow(() -> new FramesNotFoundException("The Frame by id " + id + " was not found"));
    }

    public void deleteFrame(Long id) {
        framesRepo.deleteById(id);
    }
    
}
