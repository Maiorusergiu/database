package com.example.structureapp.frames;

public class FramesNotFoundException extends RuntimeException {
    public FramesNotFoundException(String message) {
        super(message);
    }
}
