package com.example.structureapp.frames;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.example.structureapp.dots.dots;
import com.example.structureapp.movements.movements;
import com.example.structureapp.stimuli.stimuli;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
public class frames implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private long pkFrames;
    private Integer frameIndex;
    @ManyToOne
    @JoinColumn(name = "fkStimuli")
    private stimuli stimuli;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "frames")
    private List<movements> framesMovements;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "frames")
    private List<dots> framesDots;

    @JsonManagedReference(value = "framesDots")
    public List<dots> getFramesDots() {
        return framesDots;
    }
    public void setFramesDots(List<dots> framesDots) {
        this.framesDots = framesDots;
    }

    @JsonManagedReference(value = "framesMovements")
    public List<movements> getFramesMovements() {
        return framesMovements;
    }
    public void setFramesMovements(List<movements> framesMovements) {
        this.framesMovements = framesMovements;
    }





    @JsonBackReference(value = "stimuli_frames")
    public stimuli getStimuli() {
        return stimuli;
    }

    public void setStimuli(stimuli stimuli) {
        this.stimuli = stimuli;
    }


    public frames() {

    }

    public frames(Long pkFrames, Integer frameIndex) {
        this.pkFrames = pkFrames;
        this.frameIndex = frameIndex;
    }

    public Long getpkFrames() {
        return pkFrames;
    }

    public void setId(Long pkFrames) {
        this.pkFrames = pkFrames;
    }

    public Integer getFrameIndex() {
        return frameIndex;
    }
    public void setFrameIndex(Integer frameIndex) {
        this.frameIndex = frameIndex;
    }
}
