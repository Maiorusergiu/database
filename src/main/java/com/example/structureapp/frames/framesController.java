package com.example.structureapp.frames;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/frames")
public class framesController {
    private final framesService framesServ;
    @Autowired
    public framesController(framesService framesServ) {
        this.framesServ = framesServ;
    }

    @GetMapping("/all")
    public ResponseEntity<List<frames>> getAllFrames() {
        List<frames> frames = framesServ.getAllFrames();
        return new ResponseEntity<>(frames, HttpStatus.OK);
    }

    @GetMapping("find/{id}")
    public ResponseEntity<frames> getFrameById(@PathVariable("id") Long id) {
        framesServ.getFrameById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<frames> addFrame(@RequestBody frames frame){
        frames newFrame = framesServ.addFrame(frame);
        return new ResponseEntity<>(newFrame, HttpStatus.CREATED);
    }

    @PostMapping("/update")
    public ResponseEntity<frames> updateFrame(@RequestBody frames frame){
        frames updateFrame = framesServ.updateFrame(frame);
        return new ResponseEntity<>(updateFrame, HttpStatus.CREATED);
    }
    @DeleteMapping("delete/{id}")
    public ResponseEntity<frames> deleteFrame(@PathVariable("id") Long id){
        framesServ.deleteFrame(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
