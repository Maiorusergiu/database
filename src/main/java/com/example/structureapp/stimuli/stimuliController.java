package com.example.structureapp.stimuli;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stimuli")
public class stimuliController {
    private final stimuliService stimuliServ;
    @Autowired
    public stimuliController(stimuliService stimuliServ) {
        this.stimuliServ = stimuliServ;
    }

    @GetMapping("/all")
    public ResponseEntity<List<stimuli>> getAllStimuli() {
       List<stimuli> stimuli = stimuliServ.getStimuli();
       return new ResponseEntity<>(stimuli, HttpStatus.OK);
    }
    @GetMapping("find/{stimuliId}")
    public ResponseEntity<stimuli> getStimuliById(@PathVariable("stimuliId") Long stimuliId) {
        stimuliServ.getStimuliById(stimuliId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<stimuli> addStimuli(@RequestBody stimuli stimuli) {
       stimuli newStimuli = stimuliServ.addStimuli(stimuli);
       return new ResponseEntity<>(newStimuli, HttpStatus.CREATED);
    }

    @PostMapping("/update")
    public ResponseEntity<stimuli> updateStimuli(@RequestBody stimuli stimuli) {
        stimuli updateStimuli = stimuliServ.updateStimuli(stimuli);
        return new ResponseEntity<>(updateStimuli, HttpStatus.CREATED);
    }


    @DeleteMapping("delete/{id}")
    public ResponseEntity<stimuli> deleteStimuli(@PathVariable("id") Long id){
        stimuliServ.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
