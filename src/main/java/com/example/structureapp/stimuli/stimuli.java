package com.example.structureapp.stimuli;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.example.structureapp.experiments.experiments;
import com.example.structureapp.frames.frames;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
public class stimuli implements Serializable {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(nullable = false, updatable = false)
private Long pkStimuli;
private String LifeTime;
private String SColor;
private String Direction;
private Integer Delay;
private Integer Coherence;
private Integer Velocity;
private Integer Number;
@OneToMany(cascade = CascadeType.ALL, mappedBy = "stimuli")
private List<experiments> experiments;
@OneToMany(cascade = CascadeType.ALL, mappedBy = "stimuli")
private List<frames> frames;

@JsonManagedReference(value = "stimuli_frames")
public List<frames> getFrames() {
    return frames;
}

public void setFrames(List<frames> frames) {
    this.frames = frames;
}
@JsonManagedReference(value = "experiments_stimuli")
public List<experiments> getExperiments() {
    return experiments;
}

public void setExperiments(List<experiments> experiments) {
    this.experiments = experiments;
}


public stimuli() {
    
}

public stimuli(Long pkStimuli, String LifeTime, String SColor, String Direction, Integer Delay, Integer Coherence, Integer Velocity, Integer Number) {

    this.pkStimuli = pkStimuli;
    this.LifeTime = LifeTime;
    this.SColor = SColor;
    this.Direction = Direction;
    this.Delay = Delay;
    this.Coherence = Coherence;
    this.Velocity = Velocity;
    this.Number = Number;

}
public Long getPkStimuli(){
    return pkStimuli;
}

public void setPkStimuli(Long pkStimuli) {
this.pkStimuli = pkStimuli;
}

public String getLifeTime() {
    return LifeTime;
}

public void setLifeTime(String LifeTime) {
    this.LifeTime = LifeTime;
}

public String getSColor() {
    return SColor;
}

public void setSColor(String SColor) {
    this.SColor = SColor;
}

public String getDirection() {
    return Direction;
}

public void setDirection(String Direction){
    this.Direction = Direction;
}

public Integer getDelay() {
    return Delay;
}

public void setDelay(Integer Delay) {
    this.Delay = Delay;
}

public Integer getCoherence() {
    return Coherence;
}

public void setCoherence(Integer Coherence) {
    this.Coherence = Coherence;
}

public Integer getVelocity() {
    return Velocity;
}

public void setVelocity(Integer Velocity) {
    this.Velocity = Velocity;
}

public Integer getNumber() {

    return Number;
}

public void setNumber(Integer Number) {
    this.Number = Number;
    
}

}


