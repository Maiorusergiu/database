package com.example.structureapp.stimuli;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class stimuliService {

    private final stimuliRepository stimuliRepo;
    @Autowired

    public stimuliService(stimuliRepository stimuliRepo) {
        this.stimuliRepo = stimuliRepo;
    }

    public stimuli addStimuli(stimuli stimuli) {
        return stimuliRepo.save(stimuli);
    }

    public List<stimuli> getStimuli() {
        return stimuliRepo.findAll();
    }
    
    public stimuli getStimuliById(Long id) {
        return stimuliRepo.findById(id).
        orElseThrow(() -> new StimuliNotFoundException("Stimuli by id " + id + " was not found"));
    }
    public stimuli updateStimuli(stimuli stimuli) {
        return stimuliRepo.save(stimuli);
    }

    public void delete(Long id) {
        stimuliRepo.deleteById(id);
    }

}
