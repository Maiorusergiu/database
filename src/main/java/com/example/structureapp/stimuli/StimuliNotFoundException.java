package com.example.structureapp.stimuli;

public class StimuliNotFoundException extends RuntimeException {
    public StimuliNotFoundException(String message){
        super(message);
    }
}
