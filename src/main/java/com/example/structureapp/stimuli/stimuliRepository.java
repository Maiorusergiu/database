package com.example.structureapp.stimuli;

import org.springframework.data.jpa.repository.JpaRepository;

public interface stimuliRepository extends JpaRepository<stimuli, Long> {
    
}
