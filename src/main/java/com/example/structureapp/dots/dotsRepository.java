package com.example.structureapp.dots;

import org.springframework.data.jpa.repository.JpaRepository;

public interface dotsRepository extends JpaRepository<dots, Long> {
    
}
