package com.example.structureapp.dots;

public class DotsNotFoundException extends RuntimeException {
    public DotsNotFoundException(String message) {
        super(message);
    }
}
