package com.example.structureapp.dots;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class dotsService {
    private final dotsRepository dotsRepo;
    @Autowired
    public dotsService(dotsRepository dotsRepo) {
        this.dotsRepo = dotsRepo;
    }

    public List<dots> getAllDots() {
        return dotsRepo.findAll();
    }

    public dots addDots(dots dot) {
        return dotsRepo.save(dot);
    }

    public dots updateDots(dots updateDot) {
        return dotsRepo.save(updateDot);
    }

    public dots getDotById(Long id) {
        return dotsRepo.findById(id).orElseThrow(() -> new DotsNotFoundException("The dot by id " + id + " was not found"));
    }

    public void deleteDot(Long id){
        dotsRepo.deleteById(id);
    }
    
}
