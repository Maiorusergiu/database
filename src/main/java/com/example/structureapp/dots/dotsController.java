package com.example.structureapp.dots;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dots")
public class dotsController {
    private final dotsService serv;
    @Autowired
    public dotsController(dotsService serv) {
        this.serv = serv;
    }
    @GetMapping("/all")
    public ResponseEntity<List<dots>> getAllDots() {
        List<dots> dots = serv.getAllDots();
        return new ResponseEntity<>(dots, HttpStatus.OK);
    }
    @GetMapping("find/{id}")
    public ResponseEntity<dots> findDotById(@PathVariable("id") Long id) {
        dots dotById = serv.getDotById(id);
        return new ResponseEntity<>(dotById, HttpStatus.OK);
    }
    @PostMapping("/add")
    public ResponseEntity<dots> addDot(@RequestBody dots dot) {
        dots newDot = serv.addDots(dot);
        return new ResponseEntity<>(newDot, HttpStatus.CREATED);
    }
    @PostMapping("/update")
    public ResponseEntity<dots> updateDot(@RequestBody dots dot) {
        dots updateDot = serv.updateDots(dot);
        return new ResponseEntity<>(updateDot, HttpStatus.CREATED);
    }
    @DeleteMapping("delete/{id}")
    public ResponseEntity<dots> deleteDot(@PathVariable("id") Long id){
        serv.deleteDot(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
