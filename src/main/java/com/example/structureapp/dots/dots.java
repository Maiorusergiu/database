package com.example.structureapp.dots;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.example.structureapp.frames.frames;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class dots {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long pkDots;
    private Integer XCoordinate;
    private Integer YCoordinate;
    private String DColor;
    @ManyToOne
    @JoinColumn(name = "fkFrames")
    private frames frames;
    

    @JsonBackReference(value="framesDots")
    public frames getFrames() {
    return frames;
}

public void setFrames(frames frames) {
    this.frames = frames;
}
    public dots() {

    }

    public dots(Long pkDots, Integer XCoordinate, Integer YCoordinate, String DColor) {
        this.pkDots = pkDots;
        this.XCoordinate = XCoordinate;
        this.YCoordinate = YCoordinate;
        this.DColor = DColor;
    }

    public Long getpkDots() {
        return pkDots;
    }

    public void setId(Long pkDots) {
        this.pkDots = pkDots;
    }

    public Integer getXCoordinate() {
        return XCoordinate;
    }

    public void setXCoordinate(Integer XCoordinate){
        this.XCoordinate = XCoordinate;
    }

    public Integer getYCoordinate() {
        return YCoordinate;
    }

    public void setYCoordinate(Integer YCoordinate) {
        this.YCoordinate = YCoordinate;
    }

    public String getDColor() {
        return DColor;
    }

    public void setDColor(String DColor) {
        this.DColor = DColor;
    }



    
}
