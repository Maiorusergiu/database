package com.example.structureapp.participants;

import org.springframework.data.jpa.repository.JpaRepository;

public interface participantsRepository extends JpaRepository<participants, Long>{
    
}
