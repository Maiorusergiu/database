package com.example.structureapp.participants;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class participantsService {
    private final participantsRepository particpantsRepo;
    @Autowired
    public participantsService(participantsRepository participantsRepo) {
        this.particpantsRepo = participantsRepo;
    }

    public participants addParticipant(participants participant) {
        return particpantsRepo.save(participant);
    }

    public List<participants> getAllParticipants() {
        return particpantsRepo.findAll();
    }
    
    public participants updateParticipant(participants updateParticipant){
        return particpantsRepo.save(updateParticipant);
    }

    public participants getParticipantById(Long id) {
        return particpantsRepo.findById(id)
        .orElseThrow(() -> new ParticipantNotFoundException("Participant by id " + id + " was not found"));
    }
    public void deleteParticipant(Long id) {
        particpantsRepo.deleteById(id);
    }

}
