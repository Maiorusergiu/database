package com.example.structureapp.participants;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.example.structureapp.sessions.sessions;
import com.fasterxml.jackson.annotation.JsonManagedReference;



@Entity
public class participants implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long pkParticipants;
    private String Initials;
    private String Age;
    private String Sex;
    private String Information;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "participants")
    private List<sessions> sessions;

   @JsonManagedReference(value = "sessions_participants")
    public List<sessions> getSessions() {
        return sessions;
    }

    public void setSessions(List<sessions> sessions) {
        this.sessions = sessions;
    }

    public participants() {

    }

    public participants(Long pkParticipants, String Initials, String Age, String Sex, String Information) {

        this.pkParticipants = pkParticipants;
        this.Initials = Initials;
        this.Age = Age;
        this.Sex = Sex;
        this.Information = Information;

    }

    public Long getpkParticipants() {
        return pkParticipants;
    }

    public void setId (Long pkParticipants) {
        this.pkParticipants = pkParticipants;
    }

    public String getInitials() {
        return Initials;
    }

    public void setInitials(String Initials) {
        this.Initials = Initials;
    }

    public String getAge() {
        return Age;
    }
    public void setAge(String Age) {
        this.Age = Age;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String Sex) {
        this.Sex = Sex;
    }

    public String getInformation() {
        return Information;
    }

    public void setInformation(String Information) {
        this.Information = Information;
    }

    
}
