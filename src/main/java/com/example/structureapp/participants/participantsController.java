package com.example.structureapp.participants;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/participants")
public class participantsController {
    private final participantsService participantsServ;

    @Autowired
    public participantsController(participantsService participantsServ) {
        this.participantsServ = participantsServ;
    }

    @GetMapping("/all")
    public ResponseEntity<List<participants>> getAllParticipants() {
        List<participants> allParticipants = participantsServ.getAllParticipants();
        return new ResponseEntity<>(allParticipants, HttpStatus.OK);
    }

    @GetMapping("find/{id}")
    public ResponseEntity<participants> getParticipantById(@PathVariable("id") Long id){
        participantsServ.getParticipantById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<participants> addParticipant(@RequestBody participants participant) {
        participants newParticipant = participantsServ.addParticipant(participant);
        return new ResponseEntity<>(newParticipant, HttpStatus.CREATED);
    }

    @PostMapping("/update")
    public ResponseEntity<participants> updateParticipant(@RequestBody participants participant) {
        participants updateParticipant = participantsServ.addParticipant(participant);
        return new ResponseEntity<>(updateParticipant, HttpStatus.CREATED);
    }
    @DeleteMapping("delete/{id}")
    public ResponseEntity<participants> deleteParticipant(@PathVariable("id") Long id){
        participantsServ.deleteParticipant(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
